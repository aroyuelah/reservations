﻿using Hotel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace HotelData
{
    /// <summary>
    /// Guarda o carga la lista de habitaciones a persistecia
    /// </summary>
    public class RoomsData
    {
        private const string PATH = "rooms.dat";
        /// <summary>
        /// Salva la lista de habitaciones a un fichero
        /// </summary>
        /// <param name="roomList">Diccionario de habitaciones</param>
        public static void Save(Dictionary<String, Room> roomList) {
            String filename = PATH;
        
            using (FileStream sourceStream = File.Open(filename, FileMode.OpenOrCreate)){
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    formatter.Serialize(sourceStream, roomList);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                    throw;
                }  
            }

        }
        /// <summary>
        /// Carga la lista de habitaciones desde un fichero 
        /// </summary>
        /// <returns>Direccionario de habitaciones</returns>
        public static Dictionary<String, Room> Load()
        {
            Dictionary<String, Room> roomList = null;
            String filename = PATH;

            using (FileStream sourceStream = File.Open(filename, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    roomList = (Dictionary<String, Room>)formatter.Deserialize(sourceStream);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                    throw;
                }
                finally
                {
                    sourceStream.Close();
                }

            }
            return roomList;
        }

    }
}
