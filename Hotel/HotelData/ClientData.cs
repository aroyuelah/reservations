﻿using Hotel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace HotelData
{
    /// <summary>
    /// Guarda y salva los clientes en el sistema a o de ficheros persistentes
    /// </summary>
    public class ClientData
    {
        private const string PATH = "client.dat";
        /// <summary>
        /// Método para salvar la lista de clientes
        /// </summary>
        /// <param name="clientList">Una lista de clientes tipo Dictionary de String, Client </param>
        public static void Save(Dictionary<String, Client> clientList)
        {
            String filename = PATH;

            using (FileStream sourceStream = File.Open(filename, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    formatter.Serialize(sourceStream, clientList);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                    throw;
                }
            }

        }
        /// <summary>
        /// Carga de un fichero la lista de clientes
        /// </summary>
        /// <returns>Dictionario tipo string, client</returns>
        public static Dictionary<String, Client> Load()
        {
            Dictionary<String, Client> clientList = null;
            String filename = PATH;

            using (FileStream sourceStream = File.Open(filename, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    clientList = (Dictionary<String, Client>)formatter.Deserialize(sourceStream);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                    throw;
                }
                finally
                {
                    sourceStream.Close();
                }

            }
            return clientList;
        }
    }
}
