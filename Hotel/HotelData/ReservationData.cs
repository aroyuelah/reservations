﻿using Hotel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace HotelData
{/// <summary>
/// Salva o carga la lista de reservas a ficheros persistentes
/// </summary>
    public class ReservationData
    {
        private const string PATH = "reservations.dat";
        /// <summary>
        /// Guarda la lista de reservas a un fichero
        /// </summary>
        /// <param name="reservationList">Diccionario de diccionarios de reservas</param>
        public static void Save(List<Reservation> reservationList)
        {
            String filename = PATH;

            using (FileStream sourceStream = File.Open(filename, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    formatter.Serialize(sourceStream, reservationList);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                    throw;
                }
            }

        }
        /// <summary>
        /// Carga de fichero la lista de reservas guardadas
        /// </summary>
        /// <returns>Diccionario de diccionarios de reservas</returns>
        public static List<Reservation> Load()
        {
            List<Reservation> reservationList = null;
            String filename = PATH;

            using (FileStream sourceStream = File.Open(filename, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    reservationList = (List<Reservation>)formatter.Deserialize(sourceStream);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                    throw;
                }
                finally
                {
                    sourceStream.Close();
                }

            }
            return reservationList;
        }
    }
}
