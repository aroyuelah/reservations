﻿using System;
using System.Collections.Generic;

namespace Hotel
{
    /// <summary>
    /// Encapsula los datos de un cliente
    /// </summary>
    [Serializable]
    public class Client
    {
        private string id;
        private string name;
        private string contact;
        private string payment;
        private List<Bill> bills;
        private double outstanding;

        public Client()
        {
            //al final outstanding es lo que tiene pendiente por pagar pero
            //no se usa
            Outstanding = 0.0;
            Bills = new List<Bill>();
        }

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Contact { get => contact; set => contact = value; }
        public string Payment { get => payment; set => payment = value; }
        public double Outstanding { get => outstanding; set => outstanding = value; }
        internal List<Bill> Bills { get => bills; set => bills = value; }

        public override string ToString()
        {
            return "Cliente: " + Name;
        }

    }
}