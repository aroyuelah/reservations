﻿using System;

namespace Hotel
{
    /// <summary>
    /// Esta clase contendria los cargos a habitacion, descartada por contraints de tiempo
    /// </summary>
    [Serializable]
    public class Charge
    {
        private Product item;
        private uint quantity;

        public Charge(){
            Quantity = 1;
        }

        public uint Quantity { get => quantity; set => quantity = value; }
        internal Product Item { get => item; set => item = value; }

        public override string ToString()
        {
            return Item + "x" + Quantity;
        }
    }
}