﻿using System;

namespace Hotel
{
    /// <summary>
    /// Clase que contiene todos los datos sobre una reserva y sus operaciones
    /// </summary>

    [Serializable]
    public class Reservation
    {
        private Client client;
        private Room room;
        private DateTime start;
        private DateTime end;

        public DateTime Start { get => start; set => start = value; }
        public DateTime End { get => end; set => end = value; }
        public Client Client { get => client; set => client = value; }
        public Room Room { get => room; set => room = value; }
        /// <summary>
        /// Calcula el numero de noches a efectos de mostrar en pantalla
        /// </summary>
        /// <returns>Numero de noches</returns>
        public int CalculateNights()
        {
            
            int nights = ((End - Start)).Days;
            if (nights == 0)
            {
                nights = 1;
            }
            return nights;
        }

        public override string ToString()
        {
            return Client.ToString() +" " + CalculateNights().ToString() + " noches";
        }
    }
}