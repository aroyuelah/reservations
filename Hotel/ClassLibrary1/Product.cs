﻿using System;

namespace Hotel
{
    //Clase que indica el nombre y precio de un producto vendible, fuera por problemas de tiempo
    [Serializable]
    public class Product
    {
        private string name;
        private string description;
        private double price;

        public string Name { get => name; set => name = value; }
        public string Description { get => description; set => description = value; }
        public double Price { get => price; set => price = value; }

        public override string ToString()
        {
            return name;
        }
    }
}