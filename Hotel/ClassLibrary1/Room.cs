﻿using System;
using System.Collections.Generic;

namespace Hotel
{
    /// <summary>
    /// Clase que encapsula los datos de una habitación
    /// </summary>
    [Serializable]
    public class Room
    {
        private string number;
        private uint capacity;
        private List<string> extras;
        private string description;
        private double price;
        
        

        public string Number { get => number; set => number = value; }
        public uint Capacity { get => capacity; set => capacity = value; }
        public string Description { get => description; set => description = value; }
        public List<string> Extras { get => extras; set => extras = value; }
        public double Price { get => price; set => price = value; }
        

        public override string ToString()
        {
            return "Habitación "+Number+", "+Capacity+ " plazas";
        }

        
    }
}