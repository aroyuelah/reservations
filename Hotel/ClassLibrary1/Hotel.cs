﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel
{
    /// <summary>
    /// Instancia unica de nuestro hotel que contiene una lista de habitaciones
    /// clientes y reservas
    /// los productos vendibles y facturas quedan fuerta por constraints de tiempo
    /// </summary>
    [Serializable]
    public sealed class Hotel
    {
    
        private static Hotel _instance = null;

        Dictionary<string, Room> rooms;
        List<Product> products;
        Dictionary<string, Client> clients;
        List<Bill> bills;
        List<Reservation> reservations;

        private Hotel()
        {
            Rooms = new Dictionary<string, Room>();
            Products = new List<Product>();
            Clients = new Dictionary<string, Client>();
            Bills = new List<Bill>();
            //primer indice es el nº habitacion y el segundo el timespan
            Reservations = new List<Reservation>();
        }
        /// <summary>
        /// Recuperador de instancia
        /// </summary>
        public static Hotel GetInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Hotel();
                }
                return _instance;
            }
        }

        public List<Reservation> Reservations { get => reservations; set => reservations = value; }
        public Dictionary<string, Room> Rooms { get => rooms; set => rooms = value; }
        public List<Product> Products { get => products; set => products = value; }
        public Dictionary<string, Client> Clients { get => clients; set => clients = value; }
        public List<Bill> Bills { get => bills; set => bills = value; }
    }
}

