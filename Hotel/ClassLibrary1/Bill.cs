﻿using System;
using System.Collections.Generic;

namespace Hotel
{
    /// <summary>
    /// Esta clase contenia los datos y el metodo para crear una factura pero al final no
    /// se aplica por contraints de tiempo
    /// </summary>
    [Serializable]
    public class Bill
    {
        private const double TAX = 1.22;
        private const string PADDING = "\n\n";
        private Client client;
        private List<Reservation> rented;
        private List<Charge> charges;
        private DateTime date;

        internal Client Client { get => client; set => client = value; }
        internal List<Reservation> Rented { get => rented; set => rented = value; }
        internal List<Charge> Charges { get => charges; set => charges = value; }
        public DateTime Date { get => date; set => date = value; }

        public Bill()
        {
            Rented = new List<Reservation>();
            Charges = new List<Charge>();
        }

        public string GenerateBill()
        {
            Date = DateTime.Now;
            string client = Client.ToString();
            string date = Date.ToString();
            double subtotal = 0.0;
            string rented = String.Empty;
            foreach (Reservation reservation in Rented)
            {
                int nights = reservation.CalculateNights();
                double price = nights * reservation.Room.Price;
                rented = rented+"Habitacion "+reservation.Room.ToString()+" "+nights+
                    " noches - "+price+" Euros\n";
                subtotal += price;
            }
            string charges = String.Empty;
            foreach (Charge charge in Charges)
            {
                double price = charge.Item.Price * charge.Quantity;
                charges = charges+charge.ToString()+" "+price+"\n";
                subtotal += price;
            }
            string total = (subtotal * TAX).ToString();

            return date + PADDING + client + PADDING + rented + PADDING + charges + PADDING + "Subtotal: " + subtotal + "\nTotal+IVA" + total;
        }

    }
}