﻿using Hotel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelUI
{
    /// <summary>
    /// Interface grafica para crear una nueva reserva
    /// </summary>
    public partial class NewReservatioUI : Form
    {
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }

        private const string ERR_ADD_RES = "Hay que añadir un cliente, una habitacion y un intervalo de tiempo en el futuro";
        public bool edit = false;
        private Client reservationClient;
        Room reservationRoom;
        Reservation newReservation;
        Reservation editReservation;
        
        

        public Client ReservationClient { get => reservationClient; set => reservationClient = value; }
        public Reservation NewReservation { get => newReservation; set => newReservation = value; }
        public Reservation EditReservation { get => editReservation; set => editReservation = value; }

        public NewReservatioUI()
        {
           
            InitializeComponent();
        }

        //en el designer se llama asi, que se le va a hacer esto es CANCEL
        private void button4_Click(object sender, EventArgs e)
        {
            btnCancel.DialogResult = DialogResult.Cancel;
            this.Close();
        }    
        private void NewReservatioUI_Load(object sender, EventArgs e)
        {
            if (edit = true && EditReservation != null)
            {
                //inicializar campos si viene de edit
                ReservationClient = EditReservation.Client;
                txtClient.Text = EditReservation.Client.ToString();
                reservationRoom = EditReservation.Room;
                txtRoom.Text = EditReservation.Room.ToString();
                dtpStart.Value = EditReservation.Start;
                dtpFinish.Value = EditReservation.End;

            }
            else {
                //fecha de hoy y mañana
                dtpStart.Value = Start;
                dtpFinish.Value = Finish;
            }
           
        }
        /// <summary>
        /// Añade cliente a la reserva
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddClient_Click(object sender, EventArgs e)
        {
            ClientsUI addClientDiag = new ClientsUI();
            addClientDiag.ComesFromRes = true;
            addClientDiag.ShowDialog();
            if (addClientDiag.DialogResult == DialogResult.OK && addClientDiag.ClientForRes!=null) {
                ReservationClient = addClientDiag.ClientForRes;
                txtClient.Text = ReservationClient.ToString();
            }
            addClientDiag = null;
        }
        /// <summary>
        /// Añade habitacion a la reserva
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddRoom_Click(object sender, EventArgs e)
        {
            RoomsUI addRoomDiag = new RoomsUI();
            addRoomDiag.ComesFromRes = true;
            addRoomDiag.ShowDialog();
            if (addRoomDiag.DialogResult == DialogResult.OK && addRoomDiag.RoomForRes != null)
            {
                reservationRoom = addRoomDiag.RoomForRes;
                txtRoom.Text = reservationRoom.ToString();
            }
            addRoomDiag = null;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            //aqui se define la noche de 12 de la tarde a 11:59 de la mañana del dia siguiente
            TimeSpan afternoon = new TimeSpan(12, 0, 0);
            TimeSpan morning = new TimeSpan(11, 59, 0);
            DateTime start = dtpStart.Value.Date + afternoon;
            DateTime end = dtpFinish.Value.Date + morning;
            //si no estamos editando una reserva
            if (edit==false)
            {
                //No añadirá reserva si no hay habitacion o cliente o si las fechas 
                //son de inicio antes de hoy a las 12 o el fin es menor al inicio
                if (ReservationClient != null && reservationRoom != null && start >= DateTime.Now && end > start)
                {
                    NewReservation = new Reservation();
                    NewReservation.Client = ReservationClient;
                    NewReservation.Room = reservationRoom;
                    NewReservation.Start = start;
                    NewReservation.End = end;
                    this.DialogResult = DialogResult.OK;
                }
                else
                {

                    MessageBox.Show(ERR_ADD_RES);
                }
            }
            else
            {
                //si estabamos editando una reserva
                if (ReservationClient != null && reservationRoom != null && start >= DateTime.Now && end > start)
                {
                    
                    editReservation.Client = ReservationClient;
                    editReservation.Room = reservationRoom;
                    EditReservation.Start = start;
                    EditReservation.End = end;
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show(ERR_ADD_RES);
                }
            }
          
        }
    }
}
