﻿using Hotel;
using HotelData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelUI
{
    public partial class RoomsUI : Form
    {
        
        public bool ComesFromRes { get; internal set; }
        public Room RoomForRes { get; internal set; }
        

        public RoomsUI()
        {
            InitializeComponent();
        }

        private void RoomsUI_Load(object sender, EventArgs e)
        {
            RefreshRoomList();
        }
        /// <summary>
        /// Refresca la lista de habitaciones
        /// </summary>
        private void RefreshRoomList()
        {
            List<Room> roomList = new List<Room>();
            Dictionary<string, Room> roomDict = Hotel.Hotel.GetInstance.Rooms;
            foreach (Room room in roomDict.Values)
            {
                roomList.Add(room);

            }
            dgvRooms.DataSource = roomList;
        }

        /// <summary>
        /// si viene de reserva nueva, coloca una habitacion para leer a partir de la row seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            if (ComesFromRes == true && dgvRooms.SelectedRows[0] != null)
            {
                RoomForRes = (Room)dgvRooms.SelectedRows[0].DataBoundItem;
                this.DialogResult = DialogResult.OK;
            }
            
            this.Close();
        }
        /// <summary>
        /// Abre el dialogo de crear nueva habitacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            NewRoomUI newRoom = new NewRoomUI();
            newRoom.ShowDialog();

            if(newRoom.DialogResult == DialogResult.OK)
            {
                try
                {
                    Hotel.Hotel.GetInstance.Rooms.Add(newRoom.editRoom.Number, newRoom.editRoom);
                    RoomsData.Save(Hotel.Hotel.GetInstance.Rooms);
                    ReservationData.Save(Hotel.Hotel.GetInstance.Reservations);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
                finally {
                    RefreshRoomList();
                }
                
            }
        }
        /// <summary>
        /// Borra la habitacion seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            Room roomBorrar = (Room)dgvRooms.SelectedRows[0].DataBoundItem;
            Hotel.Hotel.GetInstance.Rooms.Remove(roomBorrar.Number);
            RefreshRoomList();
        }
        /// <summary>
        /// Salva los cambios a persistencia cuando se edita una habitacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvRooms_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            RoomsData.Save(Hotel.Hotel.GetInstance.Rooms);
        }
    }
}
