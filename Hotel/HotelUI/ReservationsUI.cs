﻿using Hotel;
using HotelData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelUI
{
    /// <summary>
    /// Ventana inicial que permite crear o editar reservas con doble click en las celdas
    /// contiene botones para ver clientes y habitaciones
    /// </summary>
    public partial class ReservationsUI : Form
    {
        private const int DIAS_SEMANA = 7;
        private DateTime lunes;

        public ReservationsUI()
        {
            InitializeComponent();
        }
        //Abre el editor de reservas y guarda los cambios a fichero
        private void dataGridView1_ContentCellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Reservation resForEdit = (Reservation)dgSemana.SelectedCells[0].Value;

            if (resForEdit != null)
            {
                List<Reservation> reservations = Hotel.Hotel.GetInstance.Reservations;

                Reservation res = reservations.Find(r => r.Room.Number == resForEdit.Room.Number);


                //pasar la lista
                NewReservatioUI editRes = new NewReservatioUI();
                editRes.EditReservation = res;
                editRes.edit = true;
                editRes.ShowDialog();
                if (editRes.DialogResult == DialogResult.OK)
                {
                    ReservationData.Save(Hotel.Hotel.GetInstance.Reservations);

                    RefreshReservationList();
                }
            }


        }

        private void ReservationsUI_Load(object sender, EventArgs e)
        {
            //determinar lunes semana actual
            TimeSpan middleDay = new TimeSpan(12, 0, 0);
            DateTime hoy = DateTime.Today + middleDay;
            DayOfWeek dayOfWeek = hoy.DayOfWeek;
            int numer = (int)dayOfWeek - 1;
            lunes = hoy.AddDays(-numer);
            RefreshReservationList();

        }
        /// <summary>
        /// Construye o refresca la lista de reservas mirando la lista del hotel
        /// </summary>
        private void RefreshReservationList()
        {
            dgSemana.DataSource = null;
            dgSemana.Rows.Clear();
            DateTime domingo = lunes.AddDays(6);
            //crear matriz semana
            //extraer nº habitaciones
            int roomCount = Hotel.Hotel.GetInstance.Rooms.Count;
            //construye las filas y columnas
            dgSemana.RowCount = roomCount;
            dgSemana.ColumnCount = DIAS_SEMANA;
            int k = 0;
            foreach (DataGridViewColumn column in dgSemana.Columns)
            {

                column.HeaderText = lunes.AddDays(k).ToString("dddd") + "\n" + lunes.AddDays(k).ToShortDateString();

                k++;
            }
            k = 1;
            foreach (DataGridViewRow row in dgSemana.Rows)
            {
                row.HeaderCell.Value = k.ToString();
                k++;
            }

            //lee la lista de reservas y va populando la tabla
            Dictionary<string, Room> rooms = Hotel.Hotel.GetInstance.Rooms;
            List<Reservation> reservas = Hotel.Hotel.GetInstance.Reservations;

            for (int i = 0; i < rooms.Count; i++)
            {

                for (int j = 0; j < DIAS_SEMANA; j++)
                {
                    DateTime day = lunes.AddDays(j);

                        Reservation reservation = reservas.Find(r => (r.Start <= day && r.End > day));
                        if (reservation != null)
                        {
                            dgSemana.Rows[i].Cells[j].Value = reservation;
                        }
                       
                }

            }
        }
        /// <summary>
        /// Abre la lista de habitaciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRooms_Click(object sender, EventArgs e)
        {
            RoomsUI roomsForm = new RoomsUI();


            roomsForm.ShowDialog();
            //para que, si añadimos una habitacion refresque la lista
            RefreshReservationList();

        }
        //abre la lista de clientes
        private void btnClients_Click(object sender, EventArgs e)
        {
            ClientsUI clientsForm = new ClientsUI();
            clientsForm.ShowDialog();

        }

        //abre la edicion de una reserva si hay contenido en la celda
        private void dgSemana_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            int col = dgSemana.CurrentCell.ColumnIndex;
            NewReservatioUI newReservation = new NewReservatioUI();
            newReservation.Start = lunes.AddDays(col);
            newReservation.Finish = lunes.AddDays(col + 1);
            newReservation.ShowDialog();
            if (newReservation.DialogResult == DialogResult.OK && newReservation.NewReservation != null)
            {
                Reservation reservation = newReservation.NewReservation;             
                if(!Hotel.Hotel.GetInstance.Reservations.Exists(r => r.Room.Number == reservation.Room.Number && 
                (r.Start>=reservation.Start || r.End <= reservation.End))){
                    Hotel.Hotel.GetInstance.Reservations.Add(reservation);
                    ReservationData.Save(Hotel.Hotel.GetInstance.Reservations);
                    RefreshReservationList();
                }
                else
                {
                    MessageBox.Show("La habitacíón ya está reservada en este intervalo");
                }

               
            }
        }
    
        /// <summary>
        /// muestra la semana anterior
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrvWeek_Click(object sender, EventArgs e)
        {
            lunes = lunes.AddDays(-7);
            RefreshReservationList();
        }
        /// <summary>
        /// muestra la semana siguiente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNextWeek_Click(object sender, EventArgs e)
        {
            lunes = lunes.AddDays(7);
            RefreshReservationList();
        }
        /// <summary>
        /// Elimina la reserva seleccionada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            Reservation resForRemoval = (Reservation)dgSemana.SelectedCells[0].Value;
            
            if (resForRemoval != null)
            {
                List<Reservation> reservations = Hotel.Hotel.GetInstance.Reservations;
                Reservation res = reservations.Find(r => r.Room.Number==resForRemoval.Room.Number);
                //esto deberia chequear problemas
                reservations.Remove(res);
                ReservationData.Save(Hotel.Hotel.GetInstance.Reservations);
                RefreshReservationList();
            }
        }
    }
}
