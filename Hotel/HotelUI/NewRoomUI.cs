﻿using Hotel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelUI
{
    /// <summary>
    /// Interface de nueva habitacion
    /// </summary>
    public partial class NewRoomUI : Form
    {
        private const string MSG_ERR_NUM = "Tanto precio como capacidad deben ser numeros";
        public Room editRoom = new Room();
        public NewRoomUI()
        {
            InitializeComponent();
        }


        private void btnAccept_Click(object sender, EventArgs e)
        {
            uint capacity;
            double price;
            
            editRoom.Number = txtNumber.Text;
            //comprueba que tanto capacidad comp precio sean numericos
            if (uint.TryParse(txtCapacity.Text, out capacity) && double.TryParse(txtPrice.Text, out price)){
                editRoom.Capacity = capacity;
                editRoom.Price = price;
            }
            else
            {
                MessageBox.Show(MSG_ERR_NUM);
            }
            

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
