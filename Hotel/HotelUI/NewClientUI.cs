﻿using Hotel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelUI
{
    /// <summary>
    /// Interface visual para crear nuevos clientes
    /// </summary>
    public partial class NewClientUI : Form
    {
        private const string MSG_BLANK_ID = "Id no puede estar en blanco";
        public Client editClient = new Client();

        public NewClientUI()
        {
            InitializeComponent();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (txtID.Text != String.Empty)
            {
                editClient.Name = txtName.Text;
                editClient.Id = txtID.Text;
                editClient.Payment = txtPayment.Text;
            }
            else
            {
                MessageBox.Show(MSG_BLANK_ID);
                btnAccept.DialogResult = DialogResult.Cancel;
            }
        }
    }
}
