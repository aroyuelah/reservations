﻿namespace HotelUI
{
    partial class ReservationsUI
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgSemana = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClients = new System.Windows.Forms.Button();
            this.btnRooms = new System.Windows.Forms.Button();
            this.btnPrvWeek = new System.Windows.Forms.Button();
            this.btnNextWeek = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgSemana)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgSemana
            // 
            this.dgSemana.AllowUserToAddRows = false;
            this.dgSemana.AllowUserToDeleteRows = false;
            this.dgSemana.AllowUserToResizeColumns = false;
            this.dgSemana.AllowUserToResizeRows = false;
            this.dgSemana.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgSemana.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSemana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSemana.Location = new System.Drawing.Point(0, 0);
            this.dgSemana.Name = "dgSemana";
            this.dgSemana.ReadOnly = true;
            this.dgSemana.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgSemana.Size = new System.Drawing.Size(800, 354);
            this.dgSemana.TabIndex = 0;
            this.dgSemana.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_ContentCellDoubleClick);
            this.dgSemana.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSemana_CellDoubleClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRemove);
            this.panel1.Controls.Add(this.btnNextWeek);
            this.panel1.Controls.Add(this.btnPrvWeek);
            this.panel1.Controls.Add(this.btnClients);
            this.panel1.Controls.Add(this.btnRooms);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 354);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 96);
            this.panel1.TabIndex = 1;
            // 
            // btnClients
            // 
            this.btnClients.Location = new System.Drawing.Point(99, 61);
            this.btnClients.Name = "btnClients";
            this.btnClients.Size = new System.Drawing.Size(75, 23);
            this.btnClients.TabIndex = 1;
            this.btnClients.Text = "Clientes";
            this.btnClients.UseVisualStyleBackColor = true;
            this.btnClients.Click += new System.EventHandler(this.btnClients_Click);
            // 
            // btnRooms
            // 
            this.btnRooms.Location = new System.Drawing.Point(12, 61);
            this.btnRooms.Name = "btnRooms";
            this.btnRooms.Size = new System.Drawing.Size(81, 23);
            this.btnRooms.TabIndex = 0;
            this.btnRooms.Text = "Habitaciones";
            this.btnRooms.UseVisualStyleBackColor = true;
            this.btnRooms.Click += new System.EventHandler(this.btnRooms_Click);
            // 
            // btnPrvWeek
            // 
            this.btnPrvWeek.Location = new System.Drawing.Point(12, 6);
            this.btnPrvWeek.Name = "btnPrvWeek";
            this.btnPrvWeek.Size = new System.Drawing.Size(75, 23);
            this.btnPrvWeek.TabIndex = 2;
            this.btnPrvWeek.Text = "<<";
            this.btnPrvWeek.UseVisualStyleBackColor = true;
            this.btnPrvWeek.Click += new System.EventHandler(this.btnPrvWeek_Click);
            // 
            // btnNextWeek
            // 
            this.btnNextWeek.Location = new System.Drawing.Point(713, 6);
            this.btnNextWeek.Name = "btnNextWeek";
            this.btnNextWeek.Size = new System.Drawing.Size(75, 23);
            this.btnNextWeek.TabIndex = 3;
            this.btnNextWeek.Text = ">>";
            this.btnNextWeek.UseVisualStyleBackColor = true;
            this.btnNextWeek.Click += new System.EventHandler(this.btnNextWeek_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(333, 18);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(120, 23);
            this.btnRemove.TabIndex = 4;
            this.btnRemove.Text = "Cancelar reserva";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // ReservationsUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dgSemana);
            this.Controls.Add(this.panel1);
            this.Name = "ReservationsUI";
            this.Text = "Reservas";
            this.Load += new System.EventHandler(this.ReservationsUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgSemana)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgSemana;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnRooms;
        private System.Windows.Forms.Button btnClients;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnNextWeek;
        private System.Windows.Forms.Button btnPrvWeek;
    }
}

