﻿using Hotel;
using HotelData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelUI
{
    /// <summary>
    /// User interface para mostrar y manejar clientes
    /// </summary>
    public partial class ClientsUI : Form
    {
        private const string STR_ERASE_ERR = "No se pudo eliminar nada";
        private Client clientForRes;
        private bool comesFromRes = false;
        public bool ComesFromRes { get; internal set; }
        public Client ClientForRes { get => clientForRes; set => clientForRes = value; }

        public ClientsUI()
        {
            InitializeComponent();
        }

        private void ClientsUI_Load(object sender, EventArgs e)
        {
            RefreshClientList();
            
        }
        /// <summary>
        /// Contruye la lista de clientes en el hotel
        /// </summary>
        private void RefreshClientList()
        {
            List<Client> clientList = new List<Client>();
            Dictionary<string, Client> clientDict = Hotel.Hotel.GetInstance.Clients;
            foreach (Client client in clientDict.Values)
            {
                clientList.Add(client);

            }
            dgvClients.DataSource = clientList;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Borra el cliente seleccionado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            Client clientBorrar = (Client)dgvClients.SelectedRows[0].DataBoundItem;
            try
            {
                Hotel.Hotel.GetInstance.Rooms.Remove(clientBorrar.Id);
            }
            catch (Exception)
            {
                MessageBox.Show(STR_ERASE_ERR);
            }
            finally
            {
                RefreshClientList();
            }
            
        }

        /// <summary>
        /// Detecta si viene de reserva, significando que tiene que asignar un cliente seleccionado
        /// a la variable reserva para su lectura, en caso contrario simplemente cierra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click_1(object sender, EventArgs e)
        {
            
            if (ComesFromRes == true && dgvClients.SelectedRows != null)
            {
                ClientForRes = (Client)dgvClients.SelectedRows[0].DataBoundItem;
                this.DialogResult = DialogResult.OK;
            }          
            this.Close();
        }

        /// <summary>
        /// Abre el dialogo de nuevo cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click_1(object sender, EventArgs e)
        {
            NewClientUI newClient = new NewClientUI();
            newClient.ShowDialog();

            if (newClient.DialogResult == DialogResult.OK)
            {
                //cogerá una excepcion de diccionario si esta duplicada la key
                try
                {
                    Hotel.Hotel.GetInstance.Clients.Add(newClient.editClient.Id, newClient.editClient);
                    ClientData.Save(Hotel.Hotel.GetInstance.Clients);
                    RefreshClientList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    
                }
            }
        }

        private void dgvClients_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        /// <summary>
        /// Salva los cambios cuando edita una celda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvClients_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            ClientData.Save(Hotel.Hotel.GetInstance.Clients);
        }
    }
}
