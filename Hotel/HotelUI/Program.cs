﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel;
using HotelData;

namespace HotelUI
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //crea instancia de hotel
            Hotel.Hotel myHotel = Hotel.Hotel.GetInstance;

            //popula las listas
            try
            {
                myHotel.Rooms = RoomsData.Load();
                myHotel.Clients = ClientData.Load();
                myHotel.Reservations = ReservationData.Load();
            }
            catch (Exception)
            {
                myHotel.Rooms = new Dictionary<string, Room>();
                myHotel.Clients = new Dictionary<string, Client>();
                myHotel.Reservations = new List<Reservation>();
                
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ReservationsUI());
        }
    }
}
