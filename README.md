Aplicación hotelera cc Alberto Royuela Heras 2019
Esto es una aplicación de reservas con una interface grafica para ver de un vistazo que noches estan ocupadas o libres
En la ventana de reservas se crea haciendo doble click en una celda vacía y se edita con doble click en una ocupada

La persistencia se hace con tres ficheros binarios, incluso habiendo SQL casi preferiría usar XML
en un futuro para el tema multiusuario concurrente, como por ejemplo tener el programa en recepcion
y en restaurante y tener un servlet pusheando obligatoriamente cada cambio que se hace a las terminales.

Sobre todo, lo que molaría mazo en un futuro es coger reservas en xml de webs externas y
sincronizar con xml disponibilidad de habitaciones para que no haya colisiones.